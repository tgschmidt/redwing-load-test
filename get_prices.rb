require 'ruby-jmeter'

test do
  header [{ name: 'Content-Type', value: 'application/vnd.api+json' },
          { name: 'X-Api-Key', value: 'your_token'}]

  threads count: 10,
          name: 'Get prices' do
    get name: 'get_prices_index',
        url: 'http://opdev04.dev.multiservice.com:20841/voucher/v1/prices?filter[client-pricebook-id]=pb1'
  end

  aggregate_report [{testname: 'prices_aggregate'}]
end.jmx
