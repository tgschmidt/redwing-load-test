# RedWing Rails/Passenger Load Test

Apply load to our Red Wing APIs and see how they perform.

This story was worked as part of RW-728.  The goal was to see if our version of passenger was locking up under heavy load.  TLDR; we are OK.

## Using JMeter on your local machine (Ubuntu/MacOS)

This section describes setting up JMeter on your local machine.  There are many good guides on the internet.

1. Install the most recent of version of Java packaged for your machine, if you don't already have it.
2. Install [JMeter](https://jmeter.apache.org/download_jmeter.cgi)

## Using jmeter-ruby

As the JMeter UI is quite complex, it turned out to be easier to use the [ruby-jmeter](https://github.com/flood-io/ruby-jmeter) gem, at least to create the base test case.

### Price GET example

```
require 'ruby-jmeter'

test do
  header [{ name: 'Content-Type', value: 'application/vnd.api+json' },
          { name: 'X-Api-Key', value: 'api_key_goes_here'}]

  threads count: 10,
          name: 'Get prices' do
    get name: 'get_prices_index',
        url: 'http://opdev04.dev.multiservice.com:XXXXX/voucher/v1/prices?filter[client-pricebook-id]=pb1'
  end

end.run
```

The last line can either be end.run (for running locally without the GUI) or end.jmx (to create a config to use with the JMeter GUI)

## Run it!

[Threads](https://github.com/flood-io/ruby-jmeter#threads)

You will want to specify the threads count value and likely a loops value.  To have the test run forever (until you stop it), continue_forever: true will do it.

### Command line

Assuming the end of your .rb has the "end.run", the following will run your test.

```
tgschmidt@US-8RF8JM2:~/red_wing/redwing-load-test$ bundle exec ruby get_prices.rb 
W, [2019-02-06T11:10:23.580695 #21122]  WARN -- : Test executing locally ...
I, [2019-02-06T11:11:24.821594 #21122]  INFO -- : Local Results at: jmeter.jtl
tgschmidt@US-8RF8JM2:~/red_wing/redwing-load-test$ 
```

The .jtl will have the results

```
1549473084305,181,get_prices_index,200,OK,Get prices 1-10,text,true,1969,181
1549473084305,181,get_prices_index,200,OK,Get prices 1-8,text,true,1969,181
1549473084305,181,get_prices_index,200,OK,Get prices 1-4,text,true,1969,181
1549473084305,182,get_prices_index,200,OK,Get prices 1-2,text,true,1969,182
```

### JMeter UI

Assuming the end of your .rb had the "end.jmx", the following will generate the config to utilize in the UI.

```
tgschmidt@US-8RF8JM2:~/red_wing/redwing-load-test$ bundle exec ruby get_prices.rb 
I, [2019-02-06T11:28:54.027523 #22565]  INFO -- : Test plan saved to: ruby-jmeter.jmx
tgschmidt@US-8RF8JM2:~/red_wing/redwing-load-test$
```

Fire up JMeter and load the file.  Use the execute methods in the toolbar and watch for the results!

The advantage to the UI is that setting up the Listeners is easier/more obvious that what is available in ruby-jmeter.

### Monitor Apache worker usage

While the test is running, you'll want to monitor how many Apache sessions are being used.  This is assuming on-prem testing.

http://opdev04.dev.multiservice.com:20841/server-status

### Monitor Passenger worker usage

While the test is running, you'll also want to monitor passenger thread saturation.  If running in your PWS, you'll need to first identify which passenger instance to monitor.

```bash
[tgschmidt@opdev04 red-wing]$ passenger-status 
It appears that multiple Phusion Passenger instances are running. Please
select a specific one by running:

  passenger-status <NAME>

The following Phusion Passenger instances are running:

Name                       PID      Description
--------------------------------------------------------------------------
mtaiiQxY                   20615    Apache/2.4.29 (Unix) OpenSSL/1.0.2m-fips Phusion_Passenger/5.0.30
LaZWurfM                   5052     Apache/2.2.34 (Unix) Phusion_Passenger/5.0.28
jwRdSeWQ                   4203     Apache/2.2.34 (Unix) Phusion_Passenger/5.0.28
aRfmBiqi                   4201     Apache/2.2.34 (Unix) Phusion_Passenger/5.0.28
NklqXphz                   4217     Apache/2.2.34 (Unix) Phusion_Passenger/5.0.28
```

In our case, the first one listed is the instance to monitor

```bash
[tgschmidt@opdev04 red-wing]$ passenger-status mtaiiQxY
Version : 5.0.30
Date    : 2019-01-30 15:41:24 -0600
Instance: mtaiiQxY (Apache/2.4.29 (Unix) OpenSSL/1.0.2m-fips Phusion_Passenger/5.0.30)

----------- General information -----------
Max pool size : 8
App groups    : 3
Processes     : 3
Requests in top-level queue : 0

----------- Application groups -----------
/home/tgschmidt/rw-pws/red-wing/api/voucher (development):
  App root: /home/tgschmidt/rw-pws/red-wing/api/voucher
  Requests in queue: 0
  * PID: 33201   Sessions: 0       Processed: 1       Uptime: 10m 24s
    CPU: 0%      Memory  : 81M     Last used: 10m 24s ago

/home/tgschmidt/rw-pws/red-wing/api/core (development):
  App root: /home/tgschmidt/rw-pws/red-wing/api/core
  Requests in queue: 0
  * PID: 26751   Sessions: 0       Processed: 1       Uptime: 10m 45s
    CPU: 0%      Memory  : 94M     Last used: 10m 45s ago

/home/tgschmidt/rw-pws/red-wing/api/trx (development):
  App root: /home/tgschmidt/rw-pws/red-wing/api/trx
  Requests in queue: 0
  * PID: 28093   Sessions: 0       Processed: 1       Uptime: 10m 41s
    CPU: 0%      Memory  : 63M     Last used: 10m 41s ago

[tgschmidt@opdev04 red-wing]$
```
